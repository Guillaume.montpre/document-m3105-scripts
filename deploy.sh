##configuration pour le domaine wiki.org
#configuration du serveur dwikiorg

# La commande ci-dessous crée un répertoire /etc/named sur la machine dwikiorg
himage dwikiorg mkdir -p /etc/named
# La commande ci-dessous copie le fichier se trouvant dans wiki.org/named.conf sur la machine dwikiorg dans le répertoire /etc/
hcp dwikiorg/named.conf dwikiorg:/etc/.
# La commande ci-dessous copie tous les fichiers se trouvant dans wiki.org/* dans le répertoire  /etc/named sur la machine dwikiorg
hcp dwikiorg/* dwikiorg:/etc/named/.
# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dwikiorg
himage dwikiorg rm /etc/named/named.conf



## configuration de iut.re
himage diutre mkdir -p /etc/named
hcp diutre/named.conf diutre:/etc/.
hcp diutre/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf

##configuration rt.iut.re
himage drtiutre mkdir -p /etc/named
hcp drtiutre/named.conf drtiutre:/etc/.
hcp drtiutre/* drtiutre:/etc/named/.
himage drtiutre rm /etc/named/named.conf

##configuration de dorg
himage dorg mkdir -p /etc/named
hcp dorg/named.conf dorg:/etc/.
hcp dorg/* dorg:/etc/named/.

##configuration de dre
himage dre mkdir -p /etc/named
hcp dre/named.conf dre:/etc/.
hcp dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf

##configuration de aRootServer
himage a mkdir -p /etc/named
hcp aRootServer/named.conf aRootServer:/etc/.
hcp aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf
